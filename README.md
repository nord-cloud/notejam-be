# Overview

This project is the backend application that will be used for the notejam project. It is a completely serverless golang application.

# Install dependencies
```bash
# install serverless dependencies
npm install

# Install dynamoDb locally
sls dynamodb install
```

# Build
```bash
make
```

# Run
start start dynamoDb
```bash
sls dynamodb start
```

invoke a function locally
```bash
# Note: the fetching of the docker network is necessary only for WSL
docker run --network=host --rm  -e IS_LOCAL_DEV=true -e DYNAMODB_HOST=`ip route get 1 | awk '{print $7;exit}'` -e DYNAMODB_PORT=4569 -v "$PWD":/var/task:ro,delegated lambci/lambda:go1.x bin/pads-get "`cat fixtures/get-pads-event.json`"
```
**NOTE**: When using `IS_LOCAL_DEV`, these environment variables also need to be set:
* `DYNAMODB_HOST`
* `DYNAMODB_PORT`

# Test (deprecated)
```bash
curl --request POST \
  --url http://localhost:3000/dev/ \
  --header 'content-type: application/json' \
  --header 'x-api-key: test' \
  --data '{"id": "UID-123"}'
```