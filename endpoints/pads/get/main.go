package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/google/logger"

	"gitlab.com/robert.tingirica/notejam-be/pkg/cloudservices"
	"gitlab.com/robert.tingirica/notejam-be/pkg/identity"
	"gitlab.com/robert.tingirica/notejam-be/pkg/logging"
	"gitlab.com/robert.tingirica/notejam-be/pkg/models"
	"gitlab.com/robert.tingirica/notejam-be/pkg/wrapper"
)

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context, httpEvent events.APIGatewayProxyRequest) (models.Response, error) {

	logger.Infof("handling event: %+v", httpEvent)
	userID, err := identity.GetUserIDFromEvent(&httpEvent)
	if err != nil {
		logger.Errorf("cannot determine caller from the provided token: %s", err.Error())
		return *wrapper.GenerateHTTPFailResponse(400, err, "got error determining caller identity"), nil
	}
	logger.Infof("caller is: %+v", userID)

	svc := cloudservices.NewCloudDataStoreService()

	pads, err := QueryPads(svc, userID)
	if err != nil {
		logger.Errorf("got error scanning for pads: %s", err.Error())
		return *wrapper.GenerateHTTPFailResponse(400, err, "got error scanning for pads"), nil
	}

	logger.Infof("got back %d Pads", len(*pads))

	resp := *wrapper.GenerateHTTPFetchSuccessResponse(*pads)

	return resp, nil
}

// QueryPads returns all pads for the specified user
func QueryPads(svc cloudservices.CloudDataStoreServiceI, userID string) (*[]models.Pad, error) {
	var pads = []models.Pad{}
	q_range := expression.KeyBeginsWith(expression.Key("composite"), "pad_")
	q := expression.Key("user-id").Equal(expression.Value(userID)).And(q_range)
	expr, err := expression.NewBuilder().
		WithKeyCondition(q).
		Build()

	if err != nil {
		err = fmt.Errorf("organisationStore.GetDetails: failed to build query: %v", err)
		return nil, err
	}

	result, err := svc.Query(
		&dynamodb.QueryInput{
			TableName:                 aws.String("notejam"), // TODO: Add this as a SSM parameter
			KeyConditionExpression:    expr.KeyCondition(),
			ExpressionAttributeValues: expr.Values(),
			FilterExpression:          expr.Filter(),
			ExpressionAttributeNames:  expr.Names(),
			ConsistentRead:            aws.Bool(true),
		},
	)

	if err != nil {
		return nil, err
	}

	// in case we don't have any entries in the DB we return
	if len(result.Items) == 0 {
		return &pads, nil
	}

	// in case there are objects returned, we unmarshall them into the pads object before returning
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &pads)
	if err != nil {
		return nil, err
	}

	return &pads, err
}

func main() {
	logging.ConfigureLogger()
	logger.Info("started...")
	lambda.Start(Handler)
}
