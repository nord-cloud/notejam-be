package main

import (
	"context"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/aws/aws-sdk-go/service/dynamodb/expression"
	"github.com/google/logger"

	"gitlab.com/robert.tingirica/notejam-be/pkg/cloudservices"
	"gitlab.com/robert.tingirica/notejam-be/pkg/identity"
	"gitlab.com/robert.tingirica/notejam-be/pkg/logging"
	"gitlab.com/robert.tingirica/notejam-be/pkg/models"
	"gitlab.com/robert.tingirica/notejam-be/pkg/wrapper"
)

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context, httpEvent events.APIGatewayProxyRequest) (models.Response, error) {
	logger.Infof("handling event: %+v", httpEvent)
	userID, err := identity.GetUserIDFromEvent(&httpEvent)
	if err != nil {
		logger.Errorf("cannot determine caller from the provided token: %s", err.Error())
		return *wrapper.GenerateHTTPFailResponse(400, err, "got error determining caller identity"), nil
	}
	logger.Infof("caller is: %+v", userID)

	svc := cloudservices.NewCloudDataStoreService()

	padID := httpEvent.PathParameters["id"]

	notes, err := QueryNotes(svc, userID, padID)
	if err != nil {
		logger.Errorf("got error scanning for notes: %s", err.Error())
		return *wrapper.GenerateHTTPFailResponse(400, err, "got error scanning for notes"), nil
	}

	logger.Infof("got back %d Notes", len(*notes))

	resp := *wrapper.GenerateHTTPFetchSuccessResponse(*notes)

	return resp, nil
}

// QueryNotes returns all notes for the specified user, for a specified pad
func QueryNotes(svc cloudservices.CloudDataStoreServiceI, userID, padID string) (*[]models.Note, error) {
	var notes = []models.Note{}
	q_range := expression.KeyBeginsWith(expression.Key("composite"), fmt.Sprintf("note_%s", padID))
	q := expression.Key("user-id").Equal(expression.Value(userID)).And(q_range)
	expr, err := expression.NewBuilder().
		WithKeyCondition(q).
		Build()

	if err != nil {
		err = fmt.Errorf("organisationStore.GetDetails: failed to build query: %v", err)
		return nil, err
	}

	result, err := svc.Query(
		&dynamodb.QueryInput{
			TableName:                 aws.String("notejam"), // TODO: Add this as a SSM parameter
			KeyConditionExpression:    expr.KeyCondition(),
			ExpressionAttributeValues: expr.Values(),
			FilterExpression:          expr.Filter(),
			ExpressionAttributeNames:  expr.Names(),
			ConsistentRead:            aws.Bool(true),
		},
	)

	if err != nil {
		return nil, err
	}

	// in case we don't have any entries in the DB we return
	if len(result.Items) == 0 {
		return &notes, nil
	}

	// in case there are objects returned, we unmarshall them into the notes object before returning
	err = dynamodbattribute.UnmarshalListOfMaps(result.Items, &notes)
	if err != nil {
		return nil, err
	}

	return &notes, err
}

func main() {
	logging.ConfigureLogger()
	logger.Info("started...")
	lambda.Start(Handler)
}
