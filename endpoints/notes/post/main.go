package main

import (
	"context"
	"encoding/json"
	"fmt"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
	"github.com/google/logger"

	"gitlab.com/robert.tingirica/notejam-be/pkg/cloudservices"
	"gitlab.com/robert.tingirica/notejam-be/pkg/identity"
	"gitlab.com/robert.tingirica/notejam-be/pkg/logging"
	"gitlab.com/robert.tingirica/notejam-be/pkg/models"
	"gitlab.com/robert.tingirica/notejam-be/pkg/wrapper"
)

// Handler is our lambda handler invoked by the `lambda.Start` function call
func Handler(ctx context.Context, httpEvent events.APIGatewayProxyRequest) (models.Response, error) {
	logger.Infof("handling event: %+v", httpEvent)

	userID, err := identity.GetUserIDFromEvent(&httpEvent)
	if err != nil {
		logger.Errorf("cannot determine caller from the provided token: %s", err.Error())
		return *wrapper.GenerateHTTPFailResponse(400, err, "got error determining caller identity"), nil
	}
	logger.Infof("caller is: %+v", userID)

	var note models.Note = models.Note{
		UserID: userID,
	}
	json.Unmarshal([]byte(httpEvent.Body), &note)

	padID := httpEvent.PathParameters["id"]

	note.Composite = fmt.Sprintf("note_%s_%s", padID, note.ID)

	svc := cloudservices.NewCloudDataStoreService()

	err = SaveNote(svc, &note)
	if err != nil {
		return *wrapper.GenerateHTTPFailResponse(400, err, "cannot save note"), nil
	}

	resp := *wrapper.GenerateHTTPCreateSuccessResponse("note created")

	return resp, nil
}

// SaveNote writes the note to the database
func SaveNote(svc cloudservices.CloudDataStoreServiceI, note *models.Note) error {

	av, err := dynamodbattribute.MarshalMap(note)
	if err != nil {
		return err
	}

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("notejam"),
	}
	putOutput, err := svc.PutItem(input)
	if err != nil {
		return err
	}

	logger.Infof("output from put command: %+v", putOutput)
	return nil
}

func main() {
	logging.ConfigureLogger()
	logger.Info("started...")
	lambda.Start(Handler)
}
