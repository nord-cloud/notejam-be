package models

import "github.com/aws/aws-lambda-go/events"

// Response is of type APIGatewayProxyResponse since we're leveraging the
// AWS Lambda Proxy Request functionality (default behavior)
//
// https://serverless.com/framework/docs/providers/aws/events/apigateway/#lambda-proxy-integration
type Response events.APIGatewayProxyResponse

// Pad represents the base datastructure used in defining the information needed to store notes
type Pad struct {
	Name      string `json:"name"`
	UserID    string `json:"user-id"`
	Composite string `json:"composite"`
	ID        string `json:"id"`
}

type Note struct {
	Name      string `json:"name"`
	Content   string `json:"content"`
	Composite string `json:"composite"`
	ID        string `json:"id"`
	UserID    string `json:"user-id"`
}

// HTTPResponseBase implements ClientError interface.
type HTTPResponseBase struct {
	Detail  string `json:"detail,omitempty"`
	Status  int    `json:"status,omitempty"`
	Success bool   `json:"success"`
}

// HTTPResponseFail implements ClientError interface.
type HTTPResponseFail struct {
	HTTPResponseBase
	Cause error `json:"-"`
}

// HTTPFetchSuccess implements ClientError interface.
type HTTPFetchSuccess struct {
	HTTPResponseBase
	Items interface{} `json:"items"`
}
