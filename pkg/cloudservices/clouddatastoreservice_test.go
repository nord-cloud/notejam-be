package cloudservices_test

import (
	"os"
	"testing"

	"github.com/aws/aws-sdk-go/aws"
	. "gitlab.com/robert.tingirica/notejam-be/pkg/cloudservices"
)

func TestTableGetAWSSession(t *testing.T) {
	var testCases = []struct {
		EnvVarsInput     map[string]string
		ExpectedEndpoint string
	}{
		{map[string]string{"IS_LOCAL_DEV": "true", "DYNAMODB_HOST": "localhost", "DYNAMODB_PORT": "1234"}, "http://localhost:1234"},
		{map[string]string{"IS_LOCAL_DEV": "", "DYNAMODB_HOST": "localhost", "DYNAMODB_PORT": "1234"}, ""},
	}

	for _, testCase := range testCases {
		// Set up of the environment variables
		for key, val := range testCase.EnvVarsInput {
			os.Setenv(key, val)
		}

		output := GetDynamoDBSession()
		endpointStringValue := aws.StringValue(output.Config.Endpoint)
		if endpointStringValue != testCase.ExpectedEndpoint {
			t.Errorf("Endpoint not the same: \nexpect: '%s', \nactual: '%s'\nwith environment %s", testCase.ExpectedEndpoint, endpointStringValue, testCase.EnvVarsInput)
		}

		// Cleanup of the environment variables
		for key := range testCase.EnvVarsInput {
			os.Unsetenv(key)
		}

	}
}
