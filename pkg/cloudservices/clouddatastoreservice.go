package cloudservices

import (
	"fmt"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
)

// CloudDataStoreServiceI is the base type that will be used across the project
// We use an interface so that we can mock out functions for testing
type CloudDataStoreServiceI interface {
	// dynamoDB actions
	GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error)
	PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error)
	Scan(input *dynamodb.ScanInput) (*dynamodb.ScanOutput, error)
	Query(input *dynamodb.QueryInput) (*dynamodb.QueryOutput, error)
}

// CloudDataStoreService is the structure that will get all the necessary functions and will be used for DynamoBD interactions
type CloudDataStoreService struct {
	DB *dynamodb.DynamoDB
}

// NewCloudDataStoreService creates the actual structure with the dynamoDB service present
func NewCloudDataStoreService() CloudDataStoreServiceI {
	cloudDataStoreService := CloudDataStoreService{
		DB: GetDynamoDBService(),
	}
	return &cloudDataStoreService
}

// GetItem ...
func (c *CloudDataStoreService) GetItem(input *dynamodb.GetItemInput) (*dynamodb.GetItemOutput, error) {
	return c.DB.GetItem(input)
}

// PutItem ...
func (c *CloudDataStoreService) PutItem(input *dynamodb.PutItemInput) (*dynamodb.PutItemOutput, error) {
	return c.DB.PutItem(input)
}

// Scan ...
func (c *CloudDataStoreService) Scan(input *dynamodb.ScanInput) (*dynamodb.ScanOutput, error) {
	return c.DB.Scan(input)
}

// Query ...
func (c *CloudDataStoreService) Query(input *dynamodb.QueryInput) (*dynamodb.QueryOutput, error) {
	return c.DB.Query(input)
}

// GetDynamoDBService uses the GetAWSSession to generate an AWS session, be it local or remote.
// Using this session a DynamoDB service object is created and the pointer to it returned
func GetDynamoDBService() *dynamodb.DynamoDB {
	sess := GetDynamoDBSession()
	svc := dynamodb.New(sess)
	return svc
}

// GetDynamoDBSession returns a seession to be used when creating a service object.
// In case the IS_LOCAL_DEV environment variable is set, we set the local configuration so that the functions can execute locally
func GetDynamoDBSession() *session.Session {
	// configure session options
	sessionOptions := session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}

	// we verify that we are doing local development by checking an environment variable
	isLocalDev := os.Getenv("IS_LOCAL_DEV")

	if len(isLocalDev) > 0 {
		dynamoDBHost := os.Getenv("DYNAMODB_HOST")
		dynamoDBPort := os.Getenv("DYNAMODB_PORT")

		sessionOptions.Config = aws.Config{
			Endpoint: aws.String(fmt.Sprintf("http://%s:%s", dynamoDBHost, dynamoDBPort)),
		}
	}

	// Instantiate session for DynamoDB client
	return session.Must(session.NewSessionWithOptions(sessionOptions))
}
