package wrapper

import (
	"bytes"
	"encoding/json"

	"github.com/google/logger"
	"gitlab.com/robert.tingirica/notejam-be/pkg/models"
)

// ResponseWrapper is used to add the required CORS related headers to any response we might provide back to APIGateway
func ResponseWrapper(r *models.Response) *models.Response {
	if r.Headers == nil {
		r.Headers = map[string]string{}
	}
	r.Headers["Content-Type"] = "application/json"
	r.Headers["Access-Control-Allow-Origin"] = "*"
	r.Headers["Access-Control-Allow-Credentials"] = "True"

	return r
}

// GenerateHTTPFailResponse returns a response with proper headers that can be used by API Gateway
// It also returns a human readable and reusable payload structure based on models.HTTPRequstFail
func GenerateHTTPFailResponse(statusCode int, cause error, detail string) *models.Response {
	body := models.HTTPResponseFail{
		HTTPResponseBase: models.HTTPResponseBase{
			Status:  statusCode,
			Detail:  detail,
			Success: false,
		},
		Cause: cause,
	}

	// It is Possible that we will need to check for an error here. If so, then this function would return an error.
	jsonBody, err := json.Marshal(body)
	if err != nil {
		logger.Errorf("cannot marshal error json body: %s", err)
	}

	resp := ResponseWrapper(&models.Response{
		StatusCode: statusCode,
		Body:       string(jsonBody),
	})

	return resp
}

// GenerateHTTPFetchSuccessResponse is used as the base datastructure to successfully return items to clients calling the api
// WARNING: Always return a slice when using this method! It reduces the amount of code, but is not the safest method!
func GenerateHTTPFetchSuccessResponse(items interface{}) *models.Response {
	body := models.HTTPFetchSuccess{
		Items: items,
		HTTPResponseBase: models.HTTPResponseBase{
			Success: true,
		},
	}

	// It is Possible that we will need to check for an error here. If so, then this function would return an error.
	jsonBody, err := json.Marshal(body)
	if err != nil {
		logger.Errorf("cannot marshal success json body: %s", err)
	}

	var buf bytes.Buffer

	json.HTMLEscape(&buf, jsonBody)

	resp := ResponseWrapper(&models.Response{
		StatusCode:      200,
		Body:            buf.String(),
		IsBase64Encoded: false,
	})

	return resp
}

// GenerateHTTPCreateSuccessResponse returns a response with proper headers that can be used by API Gateway
// It also returns a human readable and reusable payload structure based on models.HTTPRequestSuccess
func GenerateHTTPCreateSuccessResponse(detail string) *models.Response {
	body := models.HTTPResponseBase{
		Status:  201,
		Detail:  detail,
		Success: true,
	}

	// It is Possible that we will need to check for an error here. If so, then this function would return an error.
	jsonBody, err := json.Marshal(body)
	if err != nil {
		logger.Errorf("cannot marshal error json body: %s", err)
	}

	resp := ResponseWrapper(&models.Response{
		StatusCode: 201,
		Body:       string(jsonBody),
	})

	return resp
}
