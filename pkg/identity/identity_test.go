package identity_test

import (
	"errors"
	"testing"

	"github.com/aws/aws-lambda-go/events"
	. "gitlab.com/robert.tingirica/notejam-be/pkg/identity"
)

func TestTableGetUserFromEvent(t *testing.T) {
	var testCases = []struct {
		Description    string
		EventInput     *events.APIGatewayProxyRequest
		ExpectedUserID string
		ExpectedError  error
	}{
		{
			"normal cognito ID entry",
			&events.APIGatewayProxyRequest{
				RequestContext: events.APIGatewayProxyRequestContext{
					Identity: events.APIGatewayRequestIdentity{
						CognitoAuthenticationProvider: "cognito-idp.eu-central-1.amazonaws.com/eu-central-1_skiPyN8AP,cognito-idp.eu-central-1.amazonaws.com/eu-central-1_skiPyN8AP:CognitoSignIn:917283dd-a4c1-4c1e-b680-88a74dfd5950",
					},
				},
			},
			"917283dd-a4c1-4c1e-b680-88a74dfd5950",
			nil,
		},
		{
			"empty cognito ID entry",
			&events.APIGatewayProxyRequest{
				RequestContext: events.APIGatewayProxyRequestContext{
					Identity: events.APIGatewayRequestIdentity{
						CognitoAuthenticationProvider: "",
					},
				},
			},
			"",
			errors.New("cannot determine user id from the current authentication provider"),
		},
	}

	for _, testCase := range testCases {
		t.Logf("running: %s", testCase.Description)
		output, err := GetUserIDFromEvent(testCase.EventInput)

		if output != testCase.ExpectedUserID {
			t.Errorf("UserID not correct: \nexpect: '%s', \nactual: '%s'\nwith event: \n%+v\n", testCase.ExpectedUserID, output, testCase.EventInput)
		}

		if err != nil && testCase.ExpectedError == nil {
			t.Errorf("Error value not correct: \nexpect: '%s', \nactual: '%s'\nwith event: \n%+v\n", testCase.ExpectedError, err, testCase.EventInput)
		}
		if err == nil && testCase.ExpectedError != nil {
			t.Errorf("Error value not correct: \nexpect: '%s', \nactual: '%s'\nwith event: \n%+v\n", testCase.ExpectedError, err, testCase.EventInput)
		}

		if err != nil && testCase.ExpectedError != nil {
			if err.Error() != testCase.ExpectedError.Error() {
				t.Errorf("Error value not correct: \nexpect: '%s', \nactual: '%s'\nwith event: \n%+v\n", testCase.ExpectedError, err, testCase.EventInput)
			}
		}

	}
}
