package identity

import (
	"errors"
	"strings"

	"github.com/aws/aws-lambda-go/events"
)

// GetUserIDFromEvent ...
// We expect a string such as: cognito-idp.eu-central-1.amazonaws.com/eu-central-1_skiPyN8AP,cognito-idp.eu-central-1.amazonaws.com/eu-central-1_skiPyN8AP:CognitoSignIn:917283dd-a4c1-4c1e-b680-88a74dfd5950
// to be present under the CognitoAuthenticationProvider in the API Gateway http event object
// This function returns the user's id as it is found in Cognito
// A better name for this function might be GetSignIn
func GetUserIDFromEvent(httpEvent *events.APIGatewayProxyRequest) (string, error) {
	identityParts := strings.Split(httpEvent.RequestContext.Identity.CognitoAuthenticationProvider, "CognitoSignIn:")

	if len(identityParts) != 2 {
		return "", errors.New("cannot determine user id from the current authentication provider")
	}

	return identityParts[1], nil
}
