package logging

import (
	"io/ioutil"

	"github.com/google/logger"
)

// ConfigureLogger ...
// We do not generate a logging instance and returns a pointer to it because we get a nil pointer exception.
// The handler for the lambda function is running in a separate process which means our instance cannot be created globally.
// We call configure in `main`. Then we use the logger library call directly, which uses the root logger.
// This way the configuration can be shared
func ConfigureLogger() {
	// verboseEnvVariable := os.Getenv("INVOICE_VERBOSE")
	// verbose, err := strconv.ParseBool(verboseEnvVariable)
	// if err != nil {
	// 	verbose = false
	// }
	verbose := true

	defer logger.Init("Logger", verbose, false, ioutil.Discard).Close()
}
