module gitlab.com/robert.tingirica/notejam-be

go 1.13

require (
	github.com/aws/aws-lambda-go v1.15.0
	github.com/aws/aws-sdk-go v1.29.29
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/fnproject/fdk-go v0.0.2
	github.com/google/logger v1.1.0
	github.com/tencentyun/scf-go-lib v0.0.0-20200116145541-9a6ea1bf75b8
	golang.org/x/net v0.0.0-20200226121028-0de0cce0169b // indirect
	golang.org/x/sys v0.0.0-20190412213103-97732733099d // indirect
)
