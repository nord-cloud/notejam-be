.PHONY: build clean deploy test

build: clean test
	env GOOS=linux go build -ldflags="-s -w" -o bin/pads-post endpoints/pads/post/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/pads-get endpoints/pads/get/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/notes-get endpoints/notes/get/main.go
	env GOOS=linux go build -ldflags="-s -w" -o bin/notes-post endpoints/notes/post/main.go

clean:
	rm -rf ./bin

deploy: clean build
	sls deploy --region=eu-central-1 --stage=$(stage) --verbose

test:
	gofmt -l ./endpoints/*/*
	test -z $(gofmt -l ./endpoints/*/*)
	gofmt -l ./pkg/*
	test -z $(gofmt -l ./pkg/*)
	go vet ./endpoints/*/*
	go vet ./pkg/*
	go test -v -cover ./pkg/*
	go test -v -cover ./endpoints/*/*